module Util
( printHeader
, clear_console
, randomWord
)where

import System.Console.ANSI
import Data.List.Split
import System.Random
import System.IO


clear_console = do
	clearScreen 

printHeader :: IO ()
printHeader = do
  print $ "Universidade de Brasilia - FGA" 
  print $ "Disciplina: Paradigmas de Programacao"


selectFromList :: [[Char]] -> Int -> [Char]
selectFromList words number 
  | number == 0 = head words
  | number >= 1 = selectFromList (tail words) (number -1)

randomWord :: IO [Char]
randomWord = do
    inpStr <- readFile "words.txt"  
    let words = splitOn "\n"  inpStr
    choise  <- randomRIO (0 ,length (words)-2 )  
    return (selectFromList words choise)

