import Control.Monad ( when, unless, forM_ )
import Data.IORef ( IORef, newIORef, writeIORef, modifyIORef', atomicModifyIORef, atomicModifyIORef', atomicWriteIORef )
import Data.Maybe ( isJust )
import Graphics.UI.GLUT hiding ( initialize, Font, Front, Text )
import Graphics.Rendering.FTGL
import System.Console.GetOpt
import System.Environment ( getProgName )
import System.Exit ( exitWith, ExitCode(..) )
import System.IO ( hPutStr, stderr )
import Drawables
import GallowGame

--------------------------------------------------------------------------------
-- Setup GLUT and OpenGL, drop into the event loop.
--------------------------------------------------------------------------------
main :: IO ()
main = do
   -- Setup the basic GLUT stuff
   (_, args) <- getArgsAndInitialize
   opts <- parseOptions args
   initialDisplayMode $= [ DoubleBuffered, RGBMode, WithDepthBuffer, WithSamplesPerPixel 1 ]
   (if useFullscreen opts then fullscreenMode else windowedMode) opts

   state <- initialize

   --f <- createPolygonFont "AllerDisplay.tff"
   --setFontFaceSize f 7 7
   --renderFont f "Test" Front
   let x = (Text (Vector3 (-25) (-10) 0) "Pick another letter!" 5 (Color3 1 1 1))
   addText state x 

   -- Register the event callback functions
   displayCallback $= do render state; swapBuffers
   reshapeCallback $= Just setupProjection
   keyboardMouseCallback $= Just (keyboardMouseHandler state)
   idleCallback $= Just (postRedisplay Nothing)

   -- At this point, control is relinquished to the GLUT event handler.
   -- Control is returned as events occur, via the callback functions.
   mainLoop

fullscreenMode :: Options -> IO ()
fullscreenMode opts = do
   let addCapability c = maybe id (\x -> (Where' c IsEqualTo x :))
   gameModeCapabilities $=
      (addCapability GameModeWidth (Just (windowWidth  opts)) .
       addCapability GameModeHeight (Just (windowHeight opts)) .
       addCapability GameModeBitsPerPlane (bpp opts) .
       addCapability GameModeRefreshRate (refreshRate opts)) []
   _ <- enterGameMode
   maybeWin <- get currentWindow
   if isJust maybeWin
      then cursor $= None
      else do
         hPutStr stderr "Could not enter fullscreen mode, using windowed mode\n"
         windowedMode (opts { useFullscreen = False } )

windowedMode :: Options -> IO ()
windowedMode opts = do
   initialWindowSize $=
      Size (fromIntegral (windowWidth opts)) (fromIntegral (windowHeight opts))
   _ <- createWindow "BOGLGP - Chapter 2 - OpenGL Application"
   return ()

--------------------------------------------------------------------------------
-- Option handling
--------------------------------------------------------------------------------
data Options = Options {
   useFullscreen :: Bool,
   windowWidth   :: Int,
   windowHeight  :: Int,
   bpp           :: Maybe Int,
   refreshRate   :: Maybe Int
   }

startOpt :: Options
startOpt = Options {
   useFullscreen = False,
   windowWidth   = 800,
   windowHeight  = 600,
   bpp           = Nothing,
   refreshRate   = Nothing
   }

options :: [OptDescr (Options -> IO Options)]
options = [
   Option ['f'] ["fullscreen"]
      (NoArg (\opt -> return opt { useFullscreen = True }))
      "use fullscreen mode if possible",
   Option ['w'] ["width"]
      (ReqArg (\arg opt -> do w <- readInt "WIDTH" arg
                              return opt { windowWidth = w })
              "WIDTH")
      "use window width WIDTH",
   Option ['h'] ["height"]
      (ReqArg (\arg opt -> do h <- readInt "HEIGHT" arg
                              return opt { windowHeight = h })
              "HEIGHT")
      "use window height HEIGHT",
   Option ['b'] ["bpp"]
      (ReqArg (\arg opt -> do b <- readInt "BPP" arg
                              return opt { bpp = Just b })
              "BPP")
      "use BPP bits per plane (ignored in windowed mode)",
   Option ['r'] ["refresh-rate"]
      (ReqArg (\arg opt -> do r <- readInt "HZ" arg
                              return opt { refreshRate = Just r })
              "HZ")
      "use refresh rate HZ (ignored in windowed mode)",
   Option ['?'] ["help"]
      (NoArg (\_ -> do usage >>= putStr
                       safeExitWith ExitSuccess))
      "show help" ]

readInt :: String -> String -> IO Int
readInt name arg =
   case reads arg of
      ((x,[]) : _) -> return x
      _ -> dieWith ["Can't parse " ++ name ++ " argument '" ++ arg ++ "'\n"]

usage :: IO String
usage = do
   progName <- getProgName
   return $ usageInfo ("Usage: " ++ progName ++ " [OPTION...]") options

parseOptions :: [String] -> IO Options
parseOptions args = do
   let (optsActions, nonOptions, errs) = getOpt Permute options args
   unless (null nonOptions && null errs) (dieWith errs)
   foldl (>>=) (return startOpt) optsActions

dieWith :: [String] -> IO a
dieWith errs = do
   u <- usage
   mapM_ (hPutStr stderr) (errs ++ [u])
   safeExitWith (ExitFailure 1)

--------------------------------------------------------------------------------
-- Handle mouse and keyboard events. For this simple demo, just exit when
-- ESCAPE is pressed.
--------------------------------------------------------------------------------
keyboardMouseHandler :: State -> KeyboardMouseCallback
keyboardMouseHandler state (Char '\27') Down _ _ = safeExitWith ExitSuccess
keyboardMouseHandler state key Down _ _ = handleKeyEvent state key
keyboardMouseHandler _  _             _   _ _ = return ()

safeExitWith :: ExitCode -> IO a
safeExitWith code = do
    gma <- get gameModeActive
    when gma leaveGameMode
    exitWith code

handleKeyEvent :: State -> Key -> IO()
handleKeyEvent state key = do
   s <- get (gameState state)
   g <- get (game state)
   case s of
	   0 -> runGame state g key
	   _ -> return ()

runGame :: State -> GallowGame -> Key -> IO ()
runGame state g (Char c)
	| isWinner g = do
		clearText state
		addText state (Text (Vector3 (-10) 0 0) "You Won!" 5 (Color3 1 1 1))
		return ()
	| isLooser g = do
		clearText state
		addText state (Text (Vector3 (-12) 0 0) "You Lost!" 5 (Color3 1 1 1))
		return ()
 	| otherwise = do
		clearText state
		let nextStep = (selectLetter g c)
		addText state (Text (Vector3 (-30) 10 0) (printGallow nextStep) 5 (Color3 1 1 1))
		addText state (Text (Vector3 (-25) (-10) 0) "Pick another letter!" 5 (Color3 1 1 1))
		writeIORef (game state) nextStep
		return ()

--------------------------------------------------------------------------------
-- The globale state, which is only the current angle in this simple demo.
-- We don't need the window dimensions here, they are not used and would
-- be easily available via GLUT anyway.
	--------------------------------------------------------------------------------
data State = State {strings :: IORef [Text], gameState :: IORef Int, f :: IO Font, game :: IORef GallowGame}

makeState :: IO State
makeState = do
   list <- newIORef []
   s <- newIORef 0
   g <- newIORef (createGallow "Test" 5)
   return $ State { strings = list, gameState = s, f = (createPolygonFont "./AllerDisplay.ttf"), game = g }

--------------------------------------------------------------------------------
-- Do one time setup, i.e. set the clear color and create the global state.
--------------------------------------------------------------------------------
initialize :: IO State
initialize = do
   -- clear to black background
   clearColor $= Color4 0 0 0 0

   makeState



addText :: State -> Text -> IO ()
addText state text =  do
   atomicModifyIORef' (strings state)  (\l -> ((l ++ [text]),()))
   return ()

clearText :: State -> IO ()
clearText state = do
   atomicWriteIORef (strings state) []
   return ()
--------------------------------------------------------------------------------
-- Reset the viewport for window changes.
--------------------------------------------------------------------------------
setupProjection :: ReshapeCallback
setupProjection (Size width height) = do
   -- don't want a divide by zero
   let h = max 1 height
   -- reset the viewport to new dimensions
   viewport $= (Position 0 0, Size width h)
   -- set projection matrix as the current matrix
   matrixMode $= Projection
   -- reset projection matrix
   loadIdentity

   -- calculate aspect ratio of window
   perspective 52 (fromIntegral width / fromIntegral h) 1 1000

   -- set modelview matrix
   matrixMode $= Modelview 0
   -- reset modelview matrix
   loadIdentity

--------------------------------------------------------------------------------
-- Perform any data-specific updates for a frame. Here we only increment the
-- angle for the rotation of the triangle.
--------------------------------------------------------------------------------
--prepare :: State -> IdleCallback
--prepare state = do

--------------------------------------------------------------------------------
-- Clear and redraw the scene.
--------------------------------------------------------------------------------
render :: State -> DisplayCallback
render state = do
   -- clear screen and depth buffer
   clear [ ColorBuffer, DepthBuffer ]
   loadIdentity

   -- move back 5 units and rotate about all 3 axes
   translate (Vector3 (0::GLfloat) 0 (-5))
   scale (0.1::GLfloat) 0.1 0.1

   b <- (f state)

   displayText (Text (Vector3 (-30) 20 0) "Haskell Gallows" 3 (Color3 1 1 1)) b


   list <- get (strings state)
   forM_ list $ \text -> displayText text b


displayText :: Text -> Font -> IO ()
displayText t f = do
	preservingMatrix $ do
		translate (pos t)
		color (c t)
		setFontFaceSize f (size t) (size t)
		renderFont f (text t) Front
