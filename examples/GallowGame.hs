module GallowGame
(GallowGame
, printGallow
, printLetters 
, printAttempts
, printGameStatus
, createGallow
, selectLetter
, isWinner
, isLooser
, getAlphabet
)where

import LinkedList

data GallowGame = GG LinkedList LinkedList Int

instance Show GallowGame where
  show (GG gallow letters attempts) = show(printGallow (GG gallow letters attempts))

printGallow :: GallowGame -> [Char]
printGallow (GG gallow letters attempts) = (printLinkedList gallow)

printLetters :: GallowGame -> [Char]
printLetters (GG gallow letters attempts) = (printLinkedList letters)

printAttempts :: GallowGame -> Int
printAttempts (GG gallow letters attempts) = attempts

printGameStatus gallowGame = do
  print ("Tentativas: " ++ (show (printAttempts gallowGame)))
  print ("Opcoes: " ++ (printLetters gallowGame))
  print ("Gallow: " ++ (printGallow gallowGame))

createGallow :: [Char] -> Int -> GallowGame
createGallow word attempts = (GG (convertStringToNode word) getAlphabet attempts)

selectLetter :: GallowGame -> Char -> GallowGame
selectLetter (GG gallow letters attempts) x
  | (haveNode gallow x) = (GG (selectNode gallow x) (unselectNode letters x) (attempts))
  | otherwise = (GG gallow (unselectNode letters x) (attempts - 1))

isWinner :: GallowGame -> Bool
isWinner (GG gallow letters attempts) = (isAllNodeTrue gallow)

isLooser :: GallowGame -> Bool
isLooser (GG gallow letters attempts) = (attempts <= 0)

getAlphabet :: LinkedList 
getAlphabet = (selectAllNodeToTrue (convertStringToNode [ 'A' .. 'Z'] ))

