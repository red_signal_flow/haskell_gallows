module Drawables where
import Graphics.Rendering.OpenGL
import Graphics.UI.GLUT
import Data.IORef ( IORef, newIORef )

circle radius =  do
	2 * pi / sides
		where sides = (2 * round (2 + (log radius)))


unitCircle :: GLfloat -> [(GLfloat, GLfloat, GLfloat)]
unitCircle n = [((sin (2*pi*k/n)), cos (2*pi*k/n), 0) | k <- [1..n]]


data Text = Text { pos :: (Vector3 GLfloat), text :: String, size :: Int, c :: (Color3 GLfloat)}

data Physics = Physics { ppos :: IORef (Vector3 GLfloat), vel :: IORef (Vector3 GLfloat) }

data Circle = Circle { ophysics :: IORef Physics , radius :: IORef Int }

roundPoints _ _ 0 = []
roundPoints (x,y) radius sides = (x,y) : (roundPoints (x + radius * cos angle, y + radius * sin angle) radius (sides - 1))
	where
		angle = (2 * pi / slices) * sides
		slices = (2 * round (2 + (log radius)))
