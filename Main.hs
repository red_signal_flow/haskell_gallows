module Main where

import GallowGame
import Util
import System.IO
import Data.Char (toUpper)

play game
 | isWinner game = do
   print ("Gallow: " ++ (printGallow game))
   print "Congratulation you Win :) !!!!!"
   return False
 | isLooser game = do
   print "Sorry you Lost :( !!!!!"
   return False
 | otherwise = do
   printGameStatus game
   print $ "Choose a Letter:"
   letter <- getLine
   let x = selectLetter game (toUpper (head letter))
   play x

startGame value
 | value == True = do
  printHeader
  word <- randomWord
  result <- play (createGallow word 5)
  startGame result
 | value == False = do
  print $ "You want to play again  Y/N?"
  choise <- getLine
  if  choise == "y" || choise == "N" then startGame True else return ()


main :: IO ()
main = do
 startGame True
 print "Finished"
