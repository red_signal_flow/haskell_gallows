module LinkedList
( LinkedList(Null,Node)
, printNode
, printLinkedList
, createLinkedList
, addNode
, convertStringToNode
, selectNode
, selectAllNodeToTrue
, selectAllNodeToFalse
, unselectNode
, isAllNodeTrue
, isAllNodeFalse
, haveNode
) where

data LinkedList = Null
  | Node Char Bool LinkedList

instance Show LinkedList where
  show (Node a b next) = show(printLinkedList (Node a b next))

printNode :: LinkedList -> [Char]
printNode Null = ""
printNode (Node a b next) = [a] ++ "-" ++ (show b)

printLinkedList :: LinkedList -> [Char]
printLinkedList Null = ""
printLinkedList (Node a b next)
  | b == True = [a] ++ (printLinkedList next)
  | otherwise = "-" ++ (printLinkedList next)

createLinkedList :: Char -> LinkedList
createLinkedList a = (Node a False Null)

addNode :: LinkedList -> Char -> LinkedList
addNode Null x = (Node x False Null)
addNode (Node a b next) x = (Node a False (addNode next x))

convertStringToNode :: [Char] -> LinkedList
convertStringToNode (h:t) = (addStringToNode (createLinkedList h) t)

addStringToNode :: LinkedList -> [Char] -> LinkedList
addStringToNode (Node a b next) [] = (Node a b next)
addStringToNode (Node a b next) (h:t) = (addStringToNode (Node a b (addNode next h)) t)

selectNode :: LinkedList -> Char -> LinkedList
selectNode Null x = Null
selectNode (Node a b next) x
  | a == x = (Node a True (selectNode next x))
  | otherwise = (Node a b (selectNode next x))

unselectNode :: LinkedList -> Char -> LinkedList
unselectNode Null x = Null
unselectNode (Node a b next) x
  | a == x = (Node a False (unselectNode next x))
  | otherwise = (Node a b (unselectNode next x))

selectAllNodeToTrue  :: LinkedList -> LinkedList
selectAllNodeToTrue Null = Null
selectAllNodeToTrue (Node a b next) = (Node a True (selectAllNodeToTrue next))

selectAllNodeToFalse :: LinkedList -> LinkedList
selectAllNodeToFalse Null = Null
selectAllNodeToFalse (Node a b next) = (Node a False (selectAllNodeToFalse next))

isAllNodeTrue :: LinkedList -> Bool
isAllNodeTrue Null = True
isAllNodeTrue (Node a b next) = (b == True) && (isAllNodeTrue next)

isAllNodeFalse :: LinkedList -> Bool
isAllNodeFalse Null = True
isAllNodeFalse (Node a b next) = (b == False) && (isAllNodeFalse next)

haveNode Null x = False
haveNode (Node a b next) x
  | a == x = True
  | otherwise = (haveNode next x)
